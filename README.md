## Startracker
Este projeto tem o objetivo de executar o rastreamento dos recursos municipais.

## Desenvolvedores:


Gabriel Estevão Pires Lima, 
Iago Arantes Batista Lopes, 
Raphael dos Santos Avelar

## Executar Projeto

Para que o projeto possa ser executado em um ambiente de produção, serão necessários:


Instalação do VSCode;
Instalação do plugin C#;
Instalação da referência do MySQL;
Instalação do SGBD MySQL;
Instalação do Node e Angular (pelo VSCode);


Ao abrir o diretório do Projeto, executar o comando 'npm install' dentro do diretório para que as dependências do Projeto sejam resolvidas, para então usar o comando 'npm start' para compilar.
  Por padrão o acesso ao Projeto é feito no endereço 'http://localhost:4200'.

O projeto não está interligado em função da deficiência da estrutura técnica do local de desenvolvimento.