export class Localizacao{ // Classe que guarda os dados da Localização do Recurso, guardando a longitude e a latitude
    id: number;
    eixoX: number;
    eixoY: number;

    constructor(id: number, eixoX: number, eixoY: number) {
        this.id = id;
        this.eixoX = eixoX;
        this.eixoY = eixoY;
    }
}