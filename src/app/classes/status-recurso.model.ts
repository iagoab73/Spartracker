export class StatusRecurso{ // Classe que guarda o status do Recurso (disponível ou ocupado)
    id: number;
    descricao: string;

    constructor(id: number, descricao: string) {
        this.id = id;
        this.descricao = descricao;
    }
}