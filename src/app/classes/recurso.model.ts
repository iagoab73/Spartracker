import { StatusRecurso } from './status-recurso.model';
import { TipoRecurso } from './tipo-recurso.model';
import { Localizacao } from './localizacao.model';
import { Orgao } from './orgao.model';


export class Recurso{ // Classe contendo os atributos do Recurso, mapeados na documentação.
    id: number;
    tipoRecurso: TipoRecurso;
    localizacao: Localizacao;
    orgao: Orgao;
    status: StatusRecurso;
    ultimaAtualizacao: Date;
    marcador: google.maps.Marker;

    constructor(id: number, tipoRecurso: TipoRecurso, localizacao: Localizacao, orgao: Orgao, status: StatusRecurso, ultimaAtualizacao: Date) {
        this.id = id;
        this.tipoRecurso = tipoRecurso;
        this.localizacao = localizacao;
        this.orgao = orgao;
        this.status = status;
        this.ultimaAtualizacao = ultimaAtualizacao;
    }

    setMarcador(marcador: google.maps.Marker){
        this.marcador = marcador;
    }
}