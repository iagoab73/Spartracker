export class TipoRecurso{ // Classe que guarda o tipo de recurso (agente, automóvel)
    id: number;
    descricao: string;

    constructor(id: number, descricao: string) {
        this.id = id;
        this.descricao = descricao;
    }
}