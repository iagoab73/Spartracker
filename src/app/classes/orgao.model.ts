export class Orgao{ // Classe que guarda o Orgão ao qual o Recurso pertence
    id: number;
    descricao: string;

    constructor(id: number, descricao: string) {
        this.id = id;
        this.descricao = descricao;
    }
}