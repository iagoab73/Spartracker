import { Component, OnInit, ViewChild } from '@angular/core';

import { } from '@types/googlemaps'; // Import da API do Google Maps
import { Recurso } from '../classes/recurso.model'; // Imports das classes criadas no projeto
import { TipoRecurso } from '../classes/tipo-recurso.model';
import { Localizacao } from '../classes/localizacao.model';
import { Orgao } from '../classes/orgao.model';
import { StatusRecurso } from '../classes/status-recurso.model';

@Component({
  selector: 'app-mapa',
  templateUrl: './mapa.component.html',
  styleUrls: ['./mapa.component.css']
})
export class MapaComponent implements OnInit {
  @ViewChild('gmap') gmapElement: any;
  map: google.maps.Map; // Mapa

  title = 'SPARTRACKER'; // Título da página

  marcadoresDisponivel = ['app/marcadores/default.png', // Lista com os marcadores personalizados que seram mosrtados caso o Recurso esteja disponível
                          'app/marcadores/marcador-bhtrans-disponivel.png',
                          'app/marcadores/marcador-defesa-civil-disponivel.png',
                          'app/marcadores/marcador-guarda-municipal-disponivel.png',
                          'app/marcadores/marcador-slu-disponivel.png',
                          'app/marcadores/marcador-samu-disponivel.png',
                          'app/marcadores/marcador-sufis-disponivel.png',
                          ];
  marcadoresOcupado = ['app/marcadores/default.png', // Lista com os marcadores personalizados que seram mosrtados caso o Recurso esteja ocupado
                      'app/marcadores/marcador-bhtrans-ocupado.png',
                      'app/marcadores/marcador-defesa-civil-ocupado.png',
                      'app/marcadores/marcador-guarda-municipal-ocupado.png',
                      'app/marcadores/marcador-slu-ocupado.png',
                      'app/marcadores/marcador-samu-ocupado.png',
                      'app/marcadores/marcador-sufis-ocupado.png',
  ];
  marcadores = [this.marcadoresDisponivel, this.marcadoresOcupado]; // Lista com os arrays de marcadores
  recursos : Recurso[]; // Lista dos recursos

  constructor() { }

  ngOnInit() {
    var mapProp = { // Criando o mapa
      center: new google.maps.LatLng(-20.8543842,-42.9559953),
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    this.map = new google.maps.Map(this.gmapElement.nativeElement, mapProp); 
    this.map.setClickableIcons(true); // Seta os eventos de click dos icones como verdadeiro
    this.iniciaRecursos(); // Inicia os recursos
  }

  iniciaRecursos(){
    var bounds = new google.maps.LatLngBounds(); // Cria uma variavel de limite da área do mapa
    
    this.recursos = [
      new Recurso(1, new TipoRecurso(1,'Agente'), new Localizacao(1, -19.8635347,-43.9722852), new Orgao(3,'Guarda Municipal'), new StatusRecurso(2,'Ocupado'), new Date()),
      new Recurso(2, new TipoRecurso(2,'Automovel'), new Localizacao(2, -19.8750896,-43.9281497), new Orgao(1,'BHTRANS'), new StatusRecurso(1,'Disponivel'), new Date()),
      new Recurso(3, new TipoRecurso(3,'Estacao'), new Localizacao(3, -19.9771381,-44.024747), new Orgao(2,'Defesa Civil'), new StatusRecurso(1,'Disponivel'), new Date()),
      new Recurso(4, new TipoRecurso(1,'Agente'), new Localizacao(4, -19.9374484,-43.9726914), new Orgao(6,'SUFIS'), new StatusRecurso(1,'Disponivel'), new Date()),
      new Recurso(5, new TipoRecurso(1,'Agente'), new Localizacao(1, -19.9191248,-43.9408178), new Orgao(3,'Guarda Municipal'), new StatusRecurso(1,'Disponível'), new Date()),
      new Recurso(6, new TipoRecurso(2,'Automovel'), new Localizacao(2, -19.9320651,-43.9402159), new Orgao(5,'SAMU'), new StatusRecurso(2,'Ocupado'), new Date()),
      new Recurso(7, new TipoRecurso(3,'Estacao'), new Localizacao(3, -19.819205,-43.9766987), new Orgao(4,'SLU'), new StatusRecurso(1,'Disponivel'), new Date()),
      new Recurso(8, new TipoRecurso(1,'Agente'), new Localizacao(4, -19.85656,-43.9265867), new Orgao(6,'SUFIS'), new StatusRecurso(1,'Disponivel'), new Date()),
      new Recurso(9, new TipoRecurso(1,'Agente'), new Localizacao(1, -19.7875565,-43.9537739), new Orgao(3,'Guarda Municipal'), new StatusRecurso(1,'Disponível'), new Date()),
      new Recurso(10, new TipoRecurso(2,'Automovel'), new Localizacao(2, -19.9745572,-44.0147904), new Orgao(5,'SAMU'), new StatusRecurso(2,'Ocupado'), new Date()),
      new Recurso(11, new TipoRecurso(3,'Estacao'), new Localizacao(3, -19.9206343,-43.9223219), new Orgao(2,'Defesa Civil'), new StatusRecurso(1,'Disponivel'), new Date()),
      new Recurso(12, new TipoRecurso(1,'Agente'), new Localizacao(4, -19.9273452,-43.9300243), new Orgao(6,'SUFIS'), new StatusRecurso(1,'Disponivel'), new Date()),
    
    ];
    for(let rec of this.recursos){ // Para cada Recurso na lista
      bounds.extend(new google.maps.LatLng(rec.localizacao.eixoX, rec.localizacao.eixoY)); // Extende o limite do mapa para suprir o local do Recurso
      rec.setMarcador(new google.maps.Marker({ // Coloca o marcador no mapa
        position: new google.maps.LatLng(rec.localizacao.eixoX,rec.localizacao.eixoY),
        map: this.map,
        title: rec.orgao.descricao,
        animation: google.maps.Animation.DROP,
        icon:{ // Muda o icone do marcador com base no disponibilidade e Orgao respondavel pelo Recurso
          url: this.marcadores[(rec.status.id)-1][rec.orgao.id], 
          scaledSize: new google.maps.Size(25, 25)
    	  }
      }))
    }
    this.map.fitBounds(bounds); // Adiciona o limite feito com base nas localidades dos Recursos no mapa
  }

  mudarPosicao(rec: Recurso){ // Muda o centro do mapa para deixar um Recurso no centro (chamado ao clicar na linha de um dos elementos na tabela de Recursos)
    this.map.setCenter(rec.marcador.getPosition());
  }
  
  acaoAtribuirOcorrencia(rec: Recurso){ // Método responsável por atribuir um Recurso a uma Ocorrência (não implementado ainda pois não ter tanta relevância no problema atual)
  }
}
