import { SpartansPage } from './app.po';

describe('spartans App', function() {
  let page: SpartansPage;

  beforeEach(() => {
    page = new SpartansPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
