-- --------------------------------------------------------
-- Servidor:                     127.0.0.1
-- Versão do servidor:           5.1.65-community - MySQL Community Server (GPL)
-- OS do Servidor:               Win32
-- HeidiSQL Versão:              9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Copiando estrutura do banco de dados para spartracker
CREATE DATABASE IF NOT EXISTS `spartracker` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `spartracker`;

-- Copiando estrutura para tabela spartracker.localizacao
DROP TABLE IF EXISTS `localizacao`;
CREATE TABLE IF NOT EXISTS `localizacao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `eixo_x` decimal(16,7) NOT NULL DEFAULT '0.0000000',
  `eixo_y` decimal(16,7) NOT NULL DEFAULT '0.0000000',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela spartracker.localizacao: 1 rows
DELETE FROM `localizacao`;
/*!40000 ALTER TABLE `localizacao` DISABLE KEYS */;
INSERT INTO `localizacao` (`id`, `eixo_x`, `eixo_y`) VALUES
	(1, -19.8386814, -43.9749543);
/*!40000 ALTER TABLE `localizacao` ENABLE KEYS */;

-- Copiando estrutura para tabela spartracker.orgao
DROP TABLE IF EXISTS `orgao`;
CREATE TABLE IF NOT EXISTS `orgao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela spartracker.orgao: 1 rows
DELETE FROM `orgao`;
/*!40000 ALTER TABLE `orgao` DISABLE KEYS */;
INSERT INTO `orgao` (`id`, `descricao`) VALUES
	(1, 'SLU');
/*!40000 ALTER TABLE `orgao` ENABLE KEYS */;

-- Copiando estrutura para tabela spartracker.recursos
DROP TABLE IF EXISTS `recursos`;
CREATE TABLE IF NOT EXISTS `recursos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_hora_ultima_atualizacao` datetime DEFAULT NULL,
  `localizacao_id` int(11) NOT NULL DEFAULT '0',
  `tipo_recurso_id` int(11) NOT NULL DEFAULT '0',
  `status_recurso_id` int(11) NOT NULL DEFAULT '0',
  `orgao_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela spartracker.recursos: 1 rows
DELETE FROM `recursos`;
/*!40000 ALTER TABLE `recursos` DISABLE KEYS */;
INSERT INTO `recursos` (`id`, `data_hora_ultima_atualizacao`, `localizacao_id`, `tipo_recurso_id`, `status_recurso_id`, `orgao_id`) VALUES
	(1, '2018-06-10 06:03:00', 1, 1, 1, 1),
	(2, '2018-06-10 06:03:00', 1, 1, 1, 1);
/*!40000 ALTER TABLE `recursos` ENABLE KEYS */;

-- Copiando estrutura para tabela spartracker.status_recurso
DROP TABLE IF EXISTS `status_recurso`;
CREATE TABLE IF NOT EXISTS `status_recurso` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela spartracker.status_recurso: 1 rows
DELETE FROM `status_recurso`;
/*!40000 ALTER TABLE `status_recurso` DISABLE KEYS */;
INSERT INTO `status_recurso` (`id`, `descricao`) VALUES
	(1, 'Disponivel');
/*!40000 ALTER TABLE `status_recurso` ENABLE KEYS */;

-- Copiando estrutura para tabela spartracker.tipo_recurso
DROP TABLE IF EXISTS `tipo_recurso`;
CREATE TABLE IF NOT EXISTS `tipo_recurso` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela spartracker.tipo_recurso: 1 rows
DELETE FROM `tipo_recurso`;
/*!40000 ALTER TABLE `tipo_recurso` DISABLE KEYS */;
INSERT INTO `tipo_recurso` (`id`, `descricao`) VALUES
	(1, 'Ambulancia');
/*!40000 ALTER TABLE `tipo_recurso` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
